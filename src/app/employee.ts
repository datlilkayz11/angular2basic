export class Employee{
    employeeId:number;
    employeeName:string;
    status:boolean;
    /**
     *
     */
    constructor(id:number,name:string,status:boolean=false) {
        this.employeeId= id;
        this.employeeName = name;
        this.status = status;
    }
}