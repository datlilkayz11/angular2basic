import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  //interpolation
  public title = 'my-app';
  public name = 'DatNM8';

  //property binding 
  public image = "http://lorempixel.com/500/500";
}
