import { Component, OnInit,Input } from '@angular/core';
import {Employee} from '../employee';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  ngOnInit() {
  }
 
  //property binding
  public buttonName = "Click me!";
  public qoute:string = "If you are good at something, Never do it for free";
  public applyContent:boolean = true;

  //event binding
  OnClick(value:string){
    alert(`Button clicked ${value}`);
  }
  Toggle(){
    this.applyContent = !this.applyContent;
  }
  disableElement(){
    this.show = !this.show;
  }
  //two way binding
  public employeeName : string  = "";
  public show : boolean =  false;
  employee1 = new Employee(1,"DatNM8");
  employee2 = new Employee(2,"DatNM9",true);
  employee3 = new Employee(3,"DatNM6",true);
  employee4 = new Employee(4,"DatNM7");
  public employees:Employee[] = [this.employee1,this.employee2,this.employee3,this.employee4]

  //Input 
  @Input() name:string;
}
